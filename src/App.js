import "./App.css";
import HomeWorkPage from "./pages/HomeWork";

function App() {
  return (
    <div className="App">
      <HomeWorkPage />
    </div>
  );
}

export default App;
